package org.gvu.fidos.entity;

import java.io.Serializable;

public class MapNote implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum Type implements Serializable {
		NOTE, // User entered note
		ALERT // Dog alert
	};

	private double latitude;
	private double longitude;
	private String note;
	private Type type;

	public MapNote() {
		this(0.0, 0.0, "", Type.NOTE);
	}

	public MapNote(double latitude, double longitude, String note, Type type) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.note = note;
		this.type = type;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(latitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(longitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MapNote other = (MapNote) obj;
		if (Double.doubleToLongBits(latitude) != Double.doubleToLongBits(other.latitude))
			return false;
		if (Double.doubleToLongBits(longitude) != Double.doubleToLongBits(other.longitude))
			return false;
		if (note == null) {
			if (other.note != null)
				return false;
		} else if (!note.equals(other.note))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
}
