package org.gvu.fidos.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.gvu.fidos.MainActivity.TrackingMode;

/**
 * Data which may be persisted between application sleep/wake phases.
 */
public class PersistedState implements Serializable {
	private static final long serialVersionUID = 1L;

	final public Set<MapNote> mapNotes = new HashSet<MapNote>();
	final public PersistedLocation dogLocation = new PersistedLocation(0.0, 0.0);
	final public PersistedLocation handlerLocation = new PersistedLocation(0.0, 0.0);
	final public List<PersistedLocation> dogTrail = new ArrayList<PersistedLocation>();
	final public List<PersistedLocation> activationTrail = new ArrayList<PersistedLocation>();
	public TrackingMode trackingMode = TrackingMode.TRACK_HANDLER;
	public boolean tracking = true;
	public float zoom = 14.0f;
	public Date lastActivation = new Date();
	public Date lastMovement = new Date();
	public float cameraZoom = 0.0f;

	public void clear() {
		this.mapNotes.clear();
		dogLocation.latitude = 0.0;
		dogLocation.longitude = 0.0;
		handlerLocation.latitude = 0.0;
		handlerLocation.longitude = 0.0;
		dogTrail.clear();
		activationTrail.clear();
		trackingMode = TrackingMode.TRACK_HANDLER;
		zoom = 14.0f;
		lastActivation = new Date();
		lastMovement = new Date();
	}
}
