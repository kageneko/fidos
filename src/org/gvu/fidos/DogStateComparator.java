package org.gvu.fidos;

import java.util.Comparator;

public class DogStateComparator implements Comparator<DogState> {

	@Override
	public int compare(DogState first, DogState second) {
		return first.timestamp.compareTo(second.timestamp);
	}

}
