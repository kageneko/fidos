package org.gvu.fidos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.gvu.fidos.MainActivity.TrackingMode;
import org.gvu.fidos.entity.MapNote;
import org.gvu.fidos.entity.PersistedLocation;
import org.gvu.fidos.entity.PersistedState;

import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * A global state object. In reality, I dislike this a lot, but I was in a rush.
 * I think future versions will use message broadcasting to indicate changes.
 */
public class AppState {
	public final static int DOG_TRAIL_COLOR = 0x440088FF;
	public final static int ACTIVE_TRAIL_COLOR = 0x44FF0000;
	public final static int DEFAULT_ZOOM_LEVEL = 16;

	public final PersistedState persistedState = new PersistedState();
	public Marker dogMarker;
	public Polyline dogTrail;
	public Polyline activationTrail;
	public Marker handlerMarker;
	public CameraPosition cameraPosition;
	public boolean dogActivated = false;
	public GoogleMap map;
	public LocationClient locationClient;

	public AppState() {
		clear();
	}

	public void clear() {
		dogMarker = null;
		dogTrail = null;
		activationTrail = null;
		handlerMarker = null;
		if (map != null) {
			map.clear();
		}
		persistedState.clear();
	}

	/**
	 * Uses a simple formula to choose the stroke width for the zoom level. As
	 * we zoom in, the line should get wider.
	 */
	public float zoomToWidth() {
		float width = (persistedState.cameraZoom - 10) * 4;
		if (width < 20) {
			width = 20;
		}

		return width;
	}

	/**
	 * Update the camera position if it has not been manually changed by
	 * panning.
	 */
	public void updateCameraPosition(LatLng location) {
		CameraPosition mapPosition = map.getCameraPosition();
		// You should still be able to change zoom without affecting the camera
		// follow
		if (cameraPosition == null
				|| (mapPosition.target.latitude == cameraPosition.target.latitude && mapPosition.target.longitude == cameraPosition.target.longitude)) {
			float zoom = map.getCameraPosition().zoom;
			if (zoom < DEFAULT_ZOOM_LEVEL) {
				zoom = DEFAULT_ZOOM_LEVEL;
			}

			map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, zoom));
			cameraPosition = map.getCameraPosition();
		}
	}

	public void updateDogLocation(LatLng location) {
		this.persistedState.dogLocation.latitude = location.latitude;
		this.persistedState.dogLocation.longitude = location.longitude;

		if (dogMarker == null) {
			BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.drawable.blue_map_dog);
			dogMarker = map.addMarker(new MarkerOptions().position(location).title("Dog").icon(bitmap).alpha(.8f));
		}

		// If we have any activation trail, then the icon should be red
		if (!persistedState.activationTrail.isEmpty()) {
			dogMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_dog));
		}

		dogMarker.setPosition(location);
		dogMarker.setSnippet(String.format("[%.7f, %.7f]", location.latitude, location.longitude));

		if (persistedState.trackingMode == TrackingMode.TRACK_DOG) {
			updateCameraPosition(location);
		}
	}

	public void updateHandlerLocation(LatLng location) {
		this.persistedState.handlerLocation.latitude = location.latitude;
		this.persistedState.handlerLocation.longitude = location.longitude;

		if (handlerMarker == null) {
			handlerMarker = map.addMarker(new MarkerOptions().position(location).title("You")
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.handler_icon)).alpha(.8f));
		}
		handlerMarker.setPosition(location);
		handlerMarker.setSnippet(String.format("[%.7f, %.7f]", location.latitude, location.longitude));

		if (persistedState.trackingMode == TrackingMode.TRACK_HANDLER) {
			updateCameraPosition(location);
		}
	}

	public void addMapNote(MapNote note) {
		persistedState.mapNotes.add(note);

		String text = note.getNote();
		LatLng location = new LatLng(note.getLatitude(), note.getLongitude());
		StringBuilder snippet = new StringBuilder();

		snippet.append(String.format("[%.7f, %.7f]", location.latitude, location.longitude));

		MarkerOptions options = new MarkerOptions();
		options.alpha(.8f);
		options.position(location);

		switch (note.getType()) {
		case ALERT:
			options.title("Alert!").icon(BitmapDescriptorFactory.fromResource(R.drawable.dog_activation));
			break;
		default:
			options.title("Note").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
			break;
		}
		if (text != null && !text.isEmpty()) {
			snippet.append("\n");
			snippet.append(text);
		}
		options.snippet(snippet.toString());
		map.addMarker(options);
	}

	public void addDogTrail(LatLng... values) {
		for (LatLng location : values) {
			PersistedLocation persistedLocation = new PersistedLocation(location.latitude, location.longitude);
			persistedState.dogTrail.add(persistedLocation);
		}

		if (dogTrail == null) {
			// Create a new dog trail if we don't have one
			dogTrail = map.addPolyline(new PolylineOptions().add(values));
			dogTrail.setColor(AppState.DOG_TRAIL_COLOR);
			dogTrail.setWidth(zoomToWidth());
		} else {
			List<LatLng> points = dogTrail.getPoints();
			points.addAll(Arrays.asList(values));
			dogTrail.setPoints(points);
		}
	}

	public void addDogTrail(List<PersistedLocation> values) {
		persistedState.dogTrail.addAll(values);

		List<LatLng> locations = new ArrayList<LatLng>();
		for (PersistedLocation location : values) {
			locations.add(new LatLng(location.latitude, location.longitude));
		}

		if (dogTrail == null) {
			// Create a new dog trail if we don't have one
			dogTrail = map.addPolyline(new PolylineOptions().addAll(locations));
			dogTrail.setColor(AppState.DOG_TRAIL_COLOR);
			dogTrail.setWidth(zoomToWidth());
		} else {
			List<LatLng> points = dogTrail.getPoints();
			points.addAll(locations);
			dogTrail.setPoints(points);
		}
	}

	public void addActivationTrail(LatLng... values) {
		for (LatLng location : values) {
			PersistedLocation persistedLocation = new PersistedLocation(location.latitude, location.longitude);
			persistedState.activationTrail.add(persistedLocation);
		}

		if (activationTrail == null) {
			// Create a new dog trail if we don't have one
			activationTrail = map.addPolyline(new PolylineOptions().add(values));
			activationTrail.setColor(AppState.ACTIVE_TRAIL_COLOR);
			activationTrail.setWidth(zoomToWidth());
		} else {
			List<LatLng> points = activationTrail.getPoints();
			points.addAll(Arrays.asList(values));
			activationTrail.setPoints(points);
		}
	}

	public void addActivationTrail(List<PersistedLocation> values) {
		persistedState.activationTrail.addAll(values);

		List<LatLng> locations = new ArrayList<LatLng>();
		for (PersistedLocation location : values) {
			locations.add(new LatLng(location.latitude, location.longitude));
		}

		if (activationTrail == null) {
			// Create a new dog trail if we don't have one
			activationTrail = map.addPolyline(new PolylineOptions().addAll(locations));
			activationTrail.setColor(AppState.ACTIVE_TRAIL_COLOR);
			activationTrail.setWidth(zoomToWidth());
		} else {
			List<LatLng> points = activationTrail.getPoints();
			points.addAll(locations);
			activationTrail.setPoints(points);
		}
	}

	public boolean isActivated() {
		return !persistedState.activationTrail.isEmpty();
	}
}
