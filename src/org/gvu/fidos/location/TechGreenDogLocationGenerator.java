package org.gvu.fidos.location;

import org.gvu.fidos.AppState;
import org.gvu.fidos.DogState;
import org.gvu.fidos.MainActivity;
import org.gvu.fidos.entity.PersistedLocation;

import com.google.android.gms.maps.model.LatLng;

public class TechGreenDogLocationGenerator extends DogLocationGenerator {
	public final static double START_LATITUDE = 33.77517;
	public final static double START_LONGITUDE = -84.39690;
	public final static double TURN_POINT = 33.77406;
	public final static double END_POINT = -84.39773;
	public final static double CHANGE = 0.00005;

	private MainActivity mainActivity;

	public TechGreenDogLocationGenerator(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
	}

	@Override
	protected Void doInBackground(String... params) {
		AppState appState = mainActivity.getAppState();
		PersistedLocation location = appState.persistedState.dogLocation;
		double latitude = location.latitude;
		double longitude = location.longitude;

		if (appState.dogMarker == null) {
			latitude = START_LATITUDE;
			longitude = START_LONGITUDE;
		}

		DogState dogState = new DogState();
		if (!appState.isActivated()) {
			latitude = latitude - CHANGE;
		} else if (longitude > END_POINT) {
			longitude = longitude - (CHANGE * 2);
		} else {
			return null;
		}
		if (!appState.isActivated() && latitude <= TURN_POINT) {
			appState.persistedState.tracking = true;
			dogState.setActivated(true);
		} else {
			dogState.setActivated(false);
		}
		dogState.setLocation(new LatLng(latitude, longitude));

		this.publishProgress(dogState);
		return null;
	}

	public MainActivity getMainActivity() {
		return mainActivity;
	}
}
