package org.gvu.fidos.location;

import org.gvu.fidos.AppState;
import org.gvu.fidos.DogState;
import org.gvu.fidos.MainActivity;

import com.google.android.gms.maps.model.LatLng;

public class RandomDogLocationGenerator extends DogLocationGenerator {

	private MainActivity mainActivity;
	private AppState appState;

	public RandomDogLocationGenerator(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
		this.appState = mainActivity.getAppState();
	}

	@Override
	protected Void doInBackground(String... params) {
		if (appState.handlerMarker != null && appState.persistedState.tracking) {
			double lat = appState.persistedState.handlerLocation.latitude + (Math.random() / 1000) - .0005;
			double lng = appState.persistedState.handlerLocation.longitude + (Math.random() / 1000) - .0005;
			DogState update = new DogState();
			update.setLocation(new LatLng(lat, lng));
			update.setActivated(Math.random() < .1);
			this.publishProgress(update);
		}
		return null;
	}

	public MainActivity getMainActivity() {
		return mainActivity;
	}

}
