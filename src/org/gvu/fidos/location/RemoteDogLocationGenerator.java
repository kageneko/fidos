package org.gvu.fidos.location;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.gvu.fidos.AppState;
import org.gvu.fidos.DogState;
import org.gvu.fidos.DogStateComparator;
import org.gvu.fidos.MainActivity;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class RemoteDogLocationGenerator extends DogLocationGenerator {
	final private static String LOCATION_URL = "http://m.gatech.edu/d/lfreil3/api/fido/vests/1";
	final private static String ACTIVATION_URL = "http://m.gatech.edu/d/lfreil3/api/fido/vests/1/activations";

	private MainActivity mainActivity = null;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

	public RemoteDogLocationGenerator(MainActivity mainActivity) {
		super();
		this.mainActivity = mainActivity;
	}

	@Override
	protected MainActivity getMainActivity() {
		return mainActivity;
	}

	@Override
	protected Void doInBackground(String... params) {
		AppState appState = this.mainActivity.getAppState();

		try {
			HttpUriRequest request = new HttpGet(LOCATION_URL);
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			InputStream responseStream = response.getEntity().getContent();
			String content = readIt(responseStream, (int) response.getEntity().getContentLength());
			JSONArray locations = new JSONArray(content);
			int count = locations.length();
			Log.i("dog", "Retrieved " + locations.length() + " locations");

			// Make sure the DogStates are sorted by timestamp, since Location and Activation come in different
			// channels
			Set<DogState> newStates = new TreeSet<DogState>(new DogStateComparator());
			for (int i = 0; i < count; ++i) {
				JSONObject object = locations.getJSONObject(i);
				DogState state = new DogState();
				LatLng latLng = new LatLng(object.getDouble("lat"), object.getDouble("long"));
				String timeString = object.getString("timestamp");
				Date timestamp = dateFormat.parse(timeString);

				if (!appState.persistedState.lastMovement.before(timestamp)) {
					continue;
				}
				appState.persistedState.lastMovement = timestamp;

				Log.i("dog", String.format("Movement [%s] %f, %f", timeString, latLng.latitude, latLng.longitude));

				state.setActivated(false);
				state.setLocation(latLng);
				state.setTimestamp(timestamp);

				newStates.add(state);
			}

			try {
				responseStream.close();
			} catch (IOException e) {
				// Ignore
				// Normally use IOUtils.closeQuietly();
			}

			request = new HttpGet(ACTIVATION_URL);
			httpClient = new DefaultHttpClient();
			response = httpClient.execute(request);
			responseStream = response.getEntity().getContent();
			content = readIt(responseStream, (int) response.getEntity().getContentLength());
			JSONArray activations = new JSONArray(content);
			count = activations.length();
			Log.i("dog", "Retrieved " + activations.length() + " activations");

			for (int i = 0; i < count; ++i) {
				JSONObject object = activations.getJSONObject(i);
				DogState state = new DogState();
				LatLng latLng = new LatLng(object.getDouble("lat"), object.getDouble("long"));
				String timeString = object.getString("timestamp");
				Date timestamp = dateFormat.parse(timeString);

				Log.i("dog", timestamp.toString());
				Log.i("dog", appState.persistedState.lastActivation.toString());
				if (!appState.persistedState.lastActivation.before(timestamp)) {
					continue;
				}
				appState.persistedState.lastActivation = timestamp;

				Log.i("dog", String.format("Activation [%s] %f, %f", timeString, latLng.latitude, latLng.longitude));

				state.setActivated(true);
				state.setLocation(latLng);
				state.setTimestamp(timestamp);

				newStates.add(state);
			}

			try {
				responseStream.close();
			} catch (IOException e) {
				// Ignore
				// Normally use IOUtils.closeQuietly();
			}

			if (!newStates.isEmpty()) {
				this.publishProgress(newStates.toArray(new DogState[newStates.size()]));
			}
		} catch (Exception e) {
			Log.e("dog", e.toString(), e);
		}

		return null;
	}

	private String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
		Reader reader = null;
		reader = new InputStreamReader(stream, "UTF-8");
		char[] buffer = new char[len];
		reader.read(buffer);
		return new String(buffer);
	}
}
