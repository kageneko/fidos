package org.gvu.fidos.location;

import org.gvu.fidos.AppState;
import org.gvu.fidos.DogState;
import org.gvu.fidos.MainActivity;
import org.gvu.fidos.entity.PersistedLocation;

import com.google.android.gms.maps.model.LatLng;

/**
 * Generates a path around TSRB
 * @author Eric
 *
 */
public class TechSquareDogLocationGenerator extends DogLocationGenerator {
	public final static double START_LATITUDE = 33.77776;
	public final static double START_LONGITUDE = -84.38982;
	public final static double TURN_POINT1 = 33.77736;
	public final static double TURN_POINT2 = -84.38944;
	public final static double TURN_POINT3 = 33.77690;
	public final static double TURN_POINT4 = -84.39037;
	public final static double END_POINT = 33.77715;
	public final static double CHANGE = 0.00005;

	private MainActivity mainActivity;

	public TechSquareDogLocationGenerator(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
	}

	@Override
	protected Void doInBackground(String... params) {
		AppState appState = mainActivity.getAppState();
		PersistedLocation location = appState.persistedState.dogLocation;
		double latitude = location.latitude;
		double longitude = location.longitude;
		boolean stopped = false;

		if (appState.dogMarker == null) {
			latitude = START_LATITUDE;
			longitude = START_LONGITUDE;
		}

		DogState dogState = new DogState();
		if (latitude >= TURN_POINT3 && longitude > TURN_POINT4) {
			if (latitude > TURN_POINT1) {
				latitude = latitude - CHANGE;
			} else if (longitude < TURN_POINT2) {
				longitude = longitude + CHANGE;
			} else if (latitude > TURN_POINT3) {
				if (!appState.isActivated()) {
					dogState.setActivated(true);
				}
				latitude = latitude - (CHANGE * 2);
			} else {
				longitude = longitude - (CHANGE * 2);
			}
		} else {
			// After TURN_POINT3, every step has a small chance to cause an activation
			if ( Math.random() < .01 ) {
				dogState.setActivated(true);
			}

			if ( longitude > TURN_POINT4) {
				longitude = longitude - (CHANGE * 3);
			} else if (latitude < END_POINT) {
				latitude = latitude + (CHANGE * 3);
			} else {
				stopped = true;
			}
		}
		dogState.setLocation(new LatLng(latitude, longitude));
		
		// If the dog is not stopped or it has activated, publish the update
		if (!stopped || dogState.isActivated()) {
			this.publishProgress(dogState);
		}
		return null;
	}

	public MainActivity getMainActivity() {
		return mainActivity;
	}
}
