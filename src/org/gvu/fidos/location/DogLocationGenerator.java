package org.gvu.fidos.location;

import java.util.Locale;

import org.gvu.fidos.AppState;
import org.gvu.fidos.DogState;
import org.gvu.fidos.MainActivity;
import org.gvu.fidos.R;
import org.gvu.fidos.dialog.AlertDialogBuilder;
import org.gvu.fidos.entity.MapNote;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

/*
 * This abstract class is the superclass for all DogLocationGenerators.
 * It provides the onProgressUpdate, which takes DogState values.
 * 
 * Subclasses must implement doInBackground
 */
public abstract class DogLocationGenerator extends AsyncTask<String, DogState, Void> {
	abstract protected MainActivity getMainActivity();

	@Override
	protected void onProgressUpdate(DogState... values) {
		MainActivity mainActivity = this.getMainActivity();
		AppState appState = mainActivity.getAppState();

		for (DogState update : values) {
			LatLng location = update.getLocation();

			if (update.isActivated()) {
				Log.i("update", "Dog has activated an alert");
				// Dropper a marker at the location
				MapNote mapNote = new MapNote(location.latitude, location.longitude, "", MapNote.Type.ALERT);
				appState.addMapNote(mapNote);

				// Notify the user with an alert sound and a Toast
				// Change the dog marker to red
				appState.dogMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_map_dog));

				String message = String.format(Locale.US, "Alert from dog at [%.7f, %.7f]", location.latitude,
						location.longitude);

				if (!mainActivity.hasWindowFocus()) {
					/*
					 * If the app does not have focus, then create a
					 * notification.
					 */
					Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT);
					NotificationCompat.Builder builder = new NotificationCompat.Builder(mainActivity);

					// TODO: figure out how get the FIDO-SAR icon working here
					builder.setSmallIcon(android.R.drawable.ic_dialog_alert);
					builder.setContentText(message);

					Intent resultIntent = new Intent(mainActivity, MainActivity.class);

					// This keeps the notification from creating a new activity
					resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					PendingIntent pendingIntent = PendingIntent.getActivity(mainActivity, 0, resultIntent, 0);
					builder.setContentIntent(pendingIntent);

					// This causes the notification to disappear when activated
					builder.setAutoCancel(true);

					NotificationManager manager = (NotificationManager) mainActivity
							.getSystemService(Context.NOTIFICATION_SERVICE);
					manager.notify(0, builder.build());
				}

				AlertDialogBuilder builder = new AlertDialogBuilder(mainActivity, message);
				builder.show();

				if (!appState.isActivated()) {
					// Finish off the dog trail and start the activation trail
					appState.addDogTrail(update.getLocation());
				}
				appState.addActivationTrail(update.getLocation());
			} else {
				Log.i("dog", "Dog has moved");
				if (appState.isActivated()) {
					// If we have an activation trail, then add the points to
					// that instead of the blue dog trail
					appState.addActivationTrail(update.getLocation());
				} else {
					appState.addDogTrail(update.getLocation());
				}
			}
			appState.updateDogLocation(location);

		}

		float width = appState.zoomToWidth();
		if (appState.dogTrail != null) {
			appState.dogTrail.setWidth(width);
		}

		if (appState.activationTrail != null) {
			appState.activationTrail.setWidth(width);
		}
	}
}
