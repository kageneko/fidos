package org.gvu.fidos;

import java.util.Timer;
import java.util.TimerTask;

import org.gvu.fidos.dialog.ClearMapDialogBuilder;
import org.gvu.fidos.dialog.ErrorDialogFragment;
import org.gvu.fidos.dialog.NoteDialogBuilder;
import org.gvu.fidos.entity.PersistedState;
import org.gvu.fidos.location.DogLocationGenerator;
import org.gvu.fidos.location.TechSquareDogLocationGenerator;

import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class MainActivity extends FragmentActivity implements ConnectionCallbacks, OnConnectionFailedListener,
		LocationListener, OnMapLongClickListener, OnCameraChangeListener {

	/*
	 * Define a request code to send to Google Play services This code is
	 * returned in Activity.onActivityResult
	 */
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	// Unique tag for the error dialog fragment
	private static final String DIALOG_ERROR = "dialog_error";

	public enum TrackingMode {
		TRACK_DOG, TRACK_HANDLER;
	}

	private final AppState appState = new AppState();

	// Keep this timer around so we can cancel it if the OS decides to recreate
	// the activity (it will)
	private Timer dogTimer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i("state", "Created " + this);
		Log.i("init", "Created " + this);

		setContentView(R.layout.activity_main);

		if (savedInstanceState != null) {
			for (String key : savedInstanceState.keySet()) {
				Log.i("state", "key is " + key);
			}
			PersistedState oldState = (PersistedState) savedInstanceState.getSerializable("appState");
			if (oldState != null) {
				Log.i("state", "Restoring old appState");
				// TODO: object persistence still not working
				// appState.persistedState.activationTrail.clear();
				// appState.addActivationTrail(oldState.activationTrail);
				// appState.addDogTrail(oldState.dogTrail);
			}
		}

		this.setUpMapIfNeeded();
		appState.locationClient = new LocationClient(this, this, this);
		if (dogTimer == null) {
			startDogLocationTimer();
		}
	}

	private void startDogLocationTimer() {
		dogTimer = new Timer();
		Log.i("service", "Starting dog status update task.");
		final MainActivity mainActivity = this;
		final Handler handler = new Handler();

		// This task will query for the dog's location every second, after a
		// 10 second delay
		TimerTask doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						try {
							if (appState.persistedState.tracking) {
								// You can replace the LocationGenerator with any of a number of implementations
								DogLocationGenerator performBackgroundTask = new TechSquareDogLocationGenerator(
										mainActivity);
								performBackgroundTask.execute();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		};
		dogTimer.schedule(doAsynchronousTask, 10000, 1000);

	}

	private void setUpMapIfNeeded() {
		Log.i("init", "Setting up map");
		if (appState.map == null) {
			appState.map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
			if (appState.map != null) {
				setUpMap();
			}
		}
	}

	private void setUpMap() {
		// Map configuration
		appState.map.setIndoorEnabled(false);
		appState.map.setBuildingsEnabled(true);
		appState.map.setOnMapLongClickListener(this);
		appState.map.setOnCameraChangeListener(this);
		appState.map.getUiSettings().setRotateGesturesEnabled(false);
		appState.map.getUiSettings().setTiltGesturesEnabled(false);
	}

	protected void connectToLocationService() {
		Log.i("map", "Connecting to LocationService");
		appState.locationClient.connect();
	}

	protected void disconnectFromLocationService() {
		if (appState.locationClient != null) {
			Log.i("map", "Disconnecting from LocationService");
			appState.locationClient.disconnect();
		}
	}

	protected void stopDogTimer() {
		if (this.dogTimer != null) {
			Log.i("service", "Canceling dog stratus update task");
			this.dogTimer.cancel();
			this.dogTimer = null;
		}
	}

	protected void onStart() {
		super.onStart();
		this.connectToLocationService();

	}

	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.disconnectFromLocationService();
		this.dogTimer.cancel();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.i("state", "FIDO-SARS is being saved");

		outState.putSerializable("appState", appState.persistedState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		Log.i("state", "FIDO-SARS is being restored");

		PersistedState oldState = (PersistedState) savedInstanceState.getSerializable("appState");
		if (oldState != null) {
			Log.i("state", "Restoring old appState");
			appState.persistedState.activationTrail.clear();
			appState.addActivationTrail(oldState.activationTrail);
			appState.addDogTrail(oldState.dogTrail);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.i("state", "FIDO-SAR is paused");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i("state", "FIDO-SAR is resumed");
		setUpMapIfNeeded();
	}

	/*
	 * Button handlers
	 */
	public void changeMapMode(View view) {
		int mapType = appState.map.getMapType();
		int textResource = R.string.button_map_normal;
		// Change the map type depending on what it's already set to
		// This will also change the button label
		switch (mapType) {
		case GoogleMap.MAP_TYPE_NORMAL:
			mapType = GoogleMap.MAP_TYPE_HYBRID;
			textResource = R.string.button_map_hybrid;
			break;
		case GoogleMap.MAP_TYPE_HYBRID:
			mapType = GoogleMap.MAP_TYPE_SATELLITE;
			textResource = R.string.button_map_satellite;
			break;
		default:
			mapType = GoogleMap.MAP_TYPE_NORMAL;
			textResource = R.string.button_map_normal;
			break;
		}
		appState.map.setMapType(mapType);
		((Button) view).setText(textResource);
	}

	public void changeTracking(View view) {
		if (appState.persistedState.trackingMode == TrackingMode.TRACK_DOG) {
			changeTracking(view, TrackingMode.TRACK_HANDLER);
		} else {
			changeTracking(view, TrackingMode.TRACK_DOG);
		}
	}

	public void changeTracking(View view, TrackingMode newMode) {
		// Change the tracking mode between dog and handler
		// This affects: location update, button text, toast text
		int textResource = R.string.text_following_dog;
		int buttonResource = R.string.button_track_dog;
		LatLng position = null;
		Marker marker = null;

		if (newMode == TrackingMode.TRACK_HANDLER) {
			appState.persistedState.trackingMode = TrackingMode.TRACK_HANDLER;
			buttonResource = R.string.button_track_handler;
			textResource = R.string.text_following_handler;
			if (appState.handlerMarker != null) {
				position = appState.handlerMarker.getPosition();
			}
			marker = appState.handlerMarker;
		} else {
			appState.persistedState.trackingMode = TrackingMode.TRACK_DOG;
			buttonResource = R.string.button_track_dog;
			textResource = R.string.text_following_dog;
			if (appState.dogMarker != null) {
				position = appState.dogMarker.getPosition();
			}
			marker = appState.dogMarker;
		}

		((Button) view).setText(buttonResource);
		Toast.makeText(this, textResource, Toast.LENGTH_SHORT).show();

		if (marker != null) {
			// Move the camera to the new position
			if (appState.map != null && position != null) {
				appState.map.moveCamera(CameraUpdateFactory.newLatLngZoom(position,
						appState.map.getCameraPosition().zoom));
				appState.cameraPosition = appState.map.getCameraPosition();
			}
		}
	}

	public void addHandlerNote(View view) {
		LatLng position = null;
		if (appState.persistedState.trackingMode == TrackingMode.TRACK_HANDLER && appState.handlerMarker != null) {
			position = appState.handlerMarker.getPosition();
		} else if (appState.dogMarker != null) {
			position = appState.dogMarker.getPosition();
		}

		if (position != null) {
			NoteDialogBuilder builder = new NoteDialogBuilder(this, position);
			builder.show();
		} else {
			Log.e("location", "No location for dog or handler");
		}
	}

	public void toggleTracking(View view) {
		Button button = (Button) view;
		int buttonResource = R.string.button_stop;
		int iconResource = R.drawable.glyphicons_175_stop;
		int toastResource = R.string.text_start;

		appState.persistedState.tracking = !appState.persistedState.tracking;
		if (appState.persistedState.tracking) {
			buttonResource = R.string.button_stop;
			iconResource = R.drawable.glyphicons_175_stop;
			toastResource = R.string.text_start;
			this.connectToLocationService();
			this.startDogLocationTimer();
		} else {
			buttonResource = R.string.button_start;
			iconResource = R.drawable.glyphicons_173_play;
			toastResource = R.string.text_stop;
			this.disconnectFromLocationService();
			this.stopDogTimer();
		}

		Toast.makeText(this, toastResource, Toast.LENGTH_SHORT).show();
		button.setText(buttonResource);
		button.setCompoundDrawablesWithIntrinsicBounds(iconResource, 0, 0, 0);
	}

	public void clearMap(View view) {
		ClearMapDialogBuilder builder = new ClearMapDialogBuilder(this);
		builder.show();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		// If the Location Services connection fails, display an error dialog
		Log.e("location", "Failed to connect to location service");
		if (connectionResult.hasResolution()) {
			try {
				connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
			} catch (IntentSender.SendIntentException e) {
				e.printStackTrace();
			}
		} else {
			ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
			Bundle args = new Bundle();
			args.putInt(DIALOG_ERROR, connectionResult.getErrorCode());
			dialogFragment.setArguments(args);
			dialogFragment.show(getFragmentManager(), "errordialog");
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Log.i("location", "Connected to location service");
		LocationRequest request = LocationRequest.create();
		request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		request.setInterval(5000);
		request.setFastestInterval(1000);
		appState.locationClient.requestLocationUpdates(request, this);
	}

	@Override
	public void onDisconnected() {
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.d("location", "User location changed to " + location);
		LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());

		appState.updateHandlerLocation(ll);

		if (location.hasBearing()) {
			// Rotate the compass image
			float bearing = location.getBearing();
			Bitmap compassImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.compass);
			ImageView view = (ImageView) this.findViewById(R.id.compass);
			Matrix matrix = new Matrix();
			matrix.postRotate(bearing);
			view.setImageBitmap(Bitmap.createBitmap(compassImage, 0, 0, compassImage.getWidth(),
					compassImage.getHeight(), matrix, true));
		}
	}

	@Override
	public void onMapLongClick(final LatLng point) {
		NoteDialogBuilder builder = new NoteDialogBuilder(this, point);
		builder.show();
	}

	@Override
	public void onCameraChange(CameraPosition position) {
		appState.persistedState.cameraZoom = position.zoom;

		float width = appState.zoomToWidth();
		if (appState.dogTrail != null) {
			appState.dogTrail.setWidth(width);
		}

		if (appState.activationTrail != null) {
			appState.activationTrail.setWidth(width);
		}

		if (appState.cameraPosition != null) {
			float oldZoom = appState.cameraPosition.zoom;
			if (oldZoom != position.zoom) {
				/*
				 * The zoom changed. This messes up the map camera position a
				 * tiny bit, so we use that
				 */
				appState.cameraPosition = new CameraPosition(position.target, position.zoom,
						appState.cameraPosition.tilt, appState.cameraPosition.bearing);
			} else {
				appState.cameraPosition = new CameraPosition(appState.cameraPosition.target, position.zoom,
						appState.cameraPosition.tilt, appState.cameraPosition.bearing);
			}
		}
	}

	public AppState getAppState() {
		return appState;
	}

}
