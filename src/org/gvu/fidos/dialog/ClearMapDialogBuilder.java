package org.gvu.fidos.dialog;

import org.gvu.fidos.AppState;
import org.gvu.fidos.MainActivity;
import org.gvu.fidos.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class ClearMapDialogBuilder {
	final private MainActivity mainActivity;
	final private AppState appState;

	public ClearMapDialogBuilder(MainActivity activity) {
		this.mainActivity = activity;
		this.appState = activity.getAppState();
	}

	public void show() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.mainActivity);
		builder.setTitle("Clear Markings");
		builder.setMessage("Do you really want to clear the map?");
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setPositiveButton(android.R.string.yes, new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				appState.clear();

				// Change the tracking button to whatever is appropriate
				mainActivity.changeTracking(mainActivity.findViewById(R.id.change_tracking),
						appState.persistedState.trackingMode);
			}

		});
		builder.setNegativeButton(android.R.string.no, null);
		builder.show();
	}
}
