package org.gvu.fidos.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Displays an error dialog
 */
public class ErrorDialogFragment extends DialogFragment {
	private static final String DIALOG_ERROR = "dialog_error";
	private static final int REQUEST_RESOLVE_ERROR = 1001;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Get the error code and retrieve the appropriate dialog
		int errorCode = this.getArguments().getInt(DIALOG_ERROR);
		return GooglePlayServicesUtil.getErrorDialog(errorCode, this.getActivity(), REQUEST_RESOLVE_ERROR);
	}
}
