package org.gvu.fidos.dialog;

import org.gvu.fidos.MainActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

public class AlertDialogBuilder {
	final private MainActivity mainActivity;
	final private String message;

	public AlertDialogBuilder(MainActivity activity, String message) {
		this.mainActivity = activity;
		this.message = message;
	}

	public void show() {
		Vibrator vibrator = (Vibrator) mainActivity.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(500);
		Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Ringtone r = RingtoneManager.getRingtone(mainActivity.getApplicationContext(), notification);
		r.play();

		AlertDialog.Builder builder = new AlertDialog.Builder(this.mainActivity);
		builder.setTitle("Alert");
		builder.setMessage(message);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setPositiveButton("Ok", null);
		builder.show();
	}

}
