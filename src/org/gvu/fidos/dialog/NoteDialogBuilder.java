package org.gvu.fidos.dialog;

import org.gvu.fidos.AppState;
import org.gvu.fidos.MainActivity;
import org.gvu.fidos.entity.MapNote;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;

import com.google.android.gms.maps.model.LatLng;

public class NoteDialogBuilder {
	final private MainActivity mainActivity;
	final private AppState appState;
	final private LatLng location;

	public NoteDialogBuilder(MainActivity activity, LatLng location) {
		this.mainActivity = activity;
		this.appState = activity.getAppState();
		this.location = location;
	}

	public void show() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.mainActivity);
		builder.setTitle("Enter a note");
		final EditText input = new EditText(this.mainActivity);
		builder.setView(input);
		builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				MapNote mapNote = new MapNote(location.latitude, location.longitude, input.getText().toString(),
						MapNote.Type.NOTE);
				appState.addMapNote(mapNote);
			}
		});
		builder.setNegativeButton("Cancel", null);
		builder.show();
	}
}
