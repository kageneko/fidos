package org.gvu.fidos;

import java.util.Date;

import com.google.android.gms.maps.model.LatLng;

/*
 * The state of the dog.
 * This was supposed to be a per-update object, but I was having trouble passing messages between
 * the threads.
 */
public class DogState {
	LatLng location;
	boolean activated;
	Date timestamp = new Date();

	public LatLng getLocation() {
		return location;
	}

	public void setLocation(LatLng location) {
		this.location = location;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
}
