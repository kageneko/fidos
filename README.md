# FIDO-SAR

## Design and Architecture

This was my first Android application, so I had a lot to learn very quickly.  A lot of the sample work out there
uses a single class for everything and I copied that.  As time went on, I started moving code out into other classes
in order to make it a bit more modular.  I am pretty unhappy with my AppState class, but it was the simplest thing
for me to do at the time.  In the future, I would use a message passing mechanism and observers instead.

When MainActivity is created, it does a number of things:
- Begins a timer for monitoring the Dog Location Generator.  In the real application, this would monitor some URLs.  In this version, it sends simulated data.

- Configures the map by disabling building floorplans and setting up the listeners

- Connects to the Google Play Location Service in order to get user location data

The map will follow either the dog or the handler, depending on what is set. The user can pan the map. If that is done, automatic following is disabled until the following button is pressed.

Right now, none of the application information is persisted. This means that if the OS suspends the application, all data will be lost.

### Future Work

- Support application suspending

- Add in-app help and settings

- Change the toggle buttons to be dropdown/selection lists

- Improve the "trail size/zoom" algorithm -- it's a bit hit and miss right now

## External Resources
The sample code provided by Google Play services was used to help implement the location-aware functionality

[http://developer.android.com/training/location/index.html](http://developer.android.com/training/location/index.html)

The sample code from the Android Maps API v2 was used to implement the map details

[https://developers.google.com/maps/documentation/android/start#getting_the_google_maps_android_api_v2](https://developers.google.com/maps/documentation/android/start#getting_the_google_maps_android_api_v2)

The implementation details for NoteDialog and ClearMapDialog were inspired by
 
[http://stackoverflow.com/questions/2478517/how-to-display-a-yes-no-dialog-box-in-android/2478662](http://stackoverflow.com/questions/2478517/how-to-display-a-yes-no-dialog-box-in-android/2478662)

The polling mechanism for getting Dog Location data was inspired by

[http://stackoverflow.com/questions/9396830/android-accessing-ui-elements-from-thread](http://stackoverflow.com/questions/9396830/android-accessing-ui-elements-from-thread)

The icon set for the buttons comes from Glyphicons

[Glyphicons](http://glyphicons.com/)
